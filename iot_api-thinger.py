import requests
import pdb
import time
import json
import datetime
import pymongo
from bson import json_util, ObjectId

import asyncio
import aiohttp
from aiohttp import web
# from aiohttp_sse import sse_response
import aiohttp_cors

# ####################################################################################
# Conexao com o banco
# ####################################################################################
conexao = pymongo.MongoClient("mongodb+srv://alunos:projetointegrador@cluster0-xir76.mongodb.net/test?retryWrites=true")
mydb = conexao['gilberto']


dados = []
dados_banco = mydb.aula1.find()
print(dados_banco[0])


def grounds_def(request):
    grounds = []
    print('Ground')
    ground_banco = mydb.aula1.find({'name': 'ground'})
    for ground in ground_banco:
            grounds.append(ground)
    return web.Response(text=json_util.dumps(grounds))

def humiditys_def(request):
    humiditys = []
    print('humiditys')
    humidity_banco = mydb.aula1.find({'name': 'humidity'})
    for humidity in humidity_banco:
            humiditys.append(humidity)
    return web.Response(text=json_util.dumps(humiditys))

def temperatures_def(request):
    temperatures = []
    print('temperatures')
    temperature_banco = mydb.aula1.find({'name': 'temperature'})
    for temperature in temperature_banco:
            temperatures.append(temperature)
    return web.Response(text=json_util.dumps(temperatures))


# ####################################################################################
# CORS APP
# ####################################################################################
loop = asyncio.get_event_loop()
app = web.Application(loop=loop)

cors = aiohttp_cors.setup(app, defaults={
    "*": aiohttp_cors.ResourceOptions(
        allow_credentials=True,
        expose_headers="*",
        allow_headers="*",
    )
})


# ####################################################################################
# Routers
# ####################################################################################
grounds_router = cors.add(app.router.add_resource("/ground"))
cors.add(grounds_router.add_route("GET", grounds_def))

humiditys_router = cors.add(app.router.add_resource("/humidity"))
cors.add(humiditys_router.add_route("GET", humiditys_def))

temperatures_router = cors.add(app.router.add_resource("/temperature"))
cors.add(temperatures_router.add_route("GET", temperatures_def))

web.run_app(app)