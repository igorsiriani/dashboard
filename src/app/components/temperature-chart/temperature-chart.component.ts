import { Component, OnInit, OnChanges } from '@angular/core';
import { EChartOption } from 'echarts';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-temperature-chart',
  templateUrl: './temperature-chart.component.html',
  styleUrls: ['./temperature-chart.component.scss']
})
export class TemperatureChartComponent implements OnInit, OnChanges {

  chartOption: EChartOption = {
    title: {
      text: 'Ground Humidity',
      right: '50%',
      top: 'top'
    },
    xAxis: {
      type: 'category',
      data: []
    },
    yAxis: {
      type: 'value'
    },
    series: [{
      data: [],
      type: 'line'
    }]

  };
  mergeOption: EChartOption = { };

  date = [];
  values = [];

  constructor(private httpClient: HttpClient, private datePipe: DatePipe) {
    this.get_data()
   }

  ngOnInit() {
    // this.get_data().then(result => {
    //   this.generate_chart(result);
    // })
  }

  ngOnChanges() {
    // this.get_data().then(result => {
    //   this.generate_chart(result);
    // })
  }

  transformDate(date){
    return this.datePipe.transform(date, 'hh:mm:ss')
  }

  async get_data() {
    await this.httpClient.get('http://localhost:8080/temperature').subscribe((res: any[]) => {
      // console.log(res);
      // let d = new DatePipe()
      for(let i=0; i < res.length; i++){
        let d = this.transformDate(res[i]['date']['$date'])
        // let d = new DatePipe('en-US');
        // let date = new Date(res[i]['date']['$date'])
        console.log(res[i]['date']['$date'])
        this.date.push(d)
        this.values.push(res[i]['out'])
      }
      // console.log(this.values, this.date)
      this.generate_chart([this.date, this.values]);
    })
    return [this.date, this.values]
  }

  generate_chart(data) {
    console.log(data[0], data[1])
    // console.log(data[0][-9])
    this.mergeOption = {
      title: {
        text: 'Ground Humidity',
        right: '50%',
        top: 'top'
      },
      xAxis: {
        type: 'category',
        data: data[0]
      },
      yAxis: {
          type: 'value'
      },
      series: [{
          data: data[1],
          type: 'line'
      }]
    }

    // console.log(this.mergeOption)

  }

}
