import { Component, OnInit, Output, EventEmitter, OnChanges, Input } from '@angular/core';
import { Router } from '@angular/router';
import { faChartBar, faTable } from '@fortawesome/free-solid-svg-icons';


export interface MenuItem {
  name: string;
  path: string;
}

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  faChartBar = faChartBar;
  faTable = faTable;

  @Output()
  menuClick: EventEmitter<MenuItem> = new EventEmitter();

  toggleMenu = true;
  grid = false;
  sensors = false;

  urlChart = "";
  urlTable = "";

  constructor(private router: Router) {

    this.router.events.subscribe((res) => { 
      this.urlChart = this.router.url + '/chart';
      if(this.router.url.includes('/chart')){
        this.urlTable = this.router.url.split('/c')[0];
      } else {
        this.urlTable = this.router.url;
      }
  })
  }

  ngOnInit() {
  }

  onMenuClick(menuItem) {
    this.menuClick.emit(menuItem);
    this.router.url.includes('/chart');
  }

}
