import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroundHumidityChartComponent } from './ground-humidity-chart.component';

describe('GroundHumidityChartComponent', () => {
  let component: GroundHumidityChartComponent;
  let fixture: ComponentFixture<GroundHumidityChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroundHumidityChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroundHumidityChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
