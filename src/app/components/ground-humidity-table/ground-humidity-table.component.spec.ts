import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroundHumidityTableComponent } from './ground-humidity-table.component';

describe('GroundHumidityTableComponent', () => {
  let component: GroundHumidityTableComponent;
  let fixture: ComponentFixture<GroundHumidityTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroundHumidityTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroundHumidityTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
