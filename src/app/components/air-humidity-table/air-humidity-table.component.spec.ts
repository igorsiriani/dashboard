import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AirHumidityTableComponent } from './air-humidity-table.component';

describe('AirHumidityTableComponent', () => {
  let component: AirHumidityTableComponent;
  let fixture: ComponentFixture<AirHumidityTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AirHumidityTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AirHumidityTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
