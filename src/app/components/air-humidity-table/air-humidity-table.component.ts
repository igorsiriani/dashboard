import { Component, OnInit, OnChanges } from '@angular/core';
import { Subject, Timestamp } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';

export interface Person {
  _id: Object;
  name: string;
  date: Number;
  out: number;
}

@Component({
  selector: 'app-air-humidity-table',
  templateUrl: './air-humidity-table.component.html',
  styleUrls: ['./air-humidity-table.component.sass']
})
export class AirHumidityTableComponent implements OnInit, OnChanges {

  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  data = [];
  dtTrigger: Subject<any> = new Subject();

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get('http://localhost:8080/humidity').subscribe((res: any[]) => {
      // console.log(res)
      for(let i=0; i < res.length; i++){
        this.data.push({_id: i,
                        name: res[i]['name'],
                        date: res[i]['date']['$date'],
                        out: res[i]['out']})
      }
      this.persons = this.data;
        // Calling the DT trigger to manually render the table
      this.dtTrigger.next();
    })
  }

  ngOnChanges() {
  }

}
