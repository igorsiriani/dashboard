import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AirHumidityChartComponent } from './air-humidity-chart.component';

describe('AirHumidityChartComponent', () => {
  let component: AirHumidityChartComponent;
  let fixture: ComponentFixture<AirHumidityChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AirHumidityChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AirHumidityChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
