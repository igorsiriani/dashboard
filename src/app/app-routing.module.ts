import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TemperatureTableComponent } from './components/temperature-table/temperature-table.component';
import { TemperatureChartComponent } from './components/temperature-chart/temperature-chart.component';
import { AirHumidityTableComponent } from './components/air-humidity-table/air-humidity-table.component';
import { AirHumidityChartComponent } from './components/air-humidity-chart/air-humidity-chart.component';
import { GroundHumidityTableComponent } from './components/ground-humidity-table/ground-humidity-table.component';
import { GroundHumidityChartComponent } from './components/ground-humidity-chart/ground-humidity-chart.component';
// import { AppComponent } from "./app.component";


const routes: Routes = [
  { path: 'temperature', component: TemperatureTableComponent },
  { path: 'temperature/chart', component: TemperatureChartComponent },
  { path: 'air', component: AirHumidityTableComponent },
  { path: 'air/chart', component: AirHumidityChartComponent },
  { path: 'ground', component: GroundHumidityTableComponent },
  { path: 'ground/chart', component: GroundHumidityChartComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
