import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpClientModule } from '@angular/common/http';
import { DataTablesModule } from 'angular-datatables';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { TemperatureTableComponent } from './components/temperature-table/temperature-table.component';
import { TemperatureChartComponent } from './components/temperature-chart/temperature-chart.component';
import { AirHumidityTableComponent } from './components/air-humidity-table/air-humidity-table.component';
import { AirHumidityChartComponent } from './components/air-humidity-chart/air-humidity-chart.component';
import { GroundHumidityTableComponent } from './components/ground-humidity-table/ground-humidity-table.component';
import { GroundHumidityChartComponent } from './components/ground-humidity-chart/ground-humidity-chart.component';
import { DatePipe } from '@angular/common';

// import { AngularmaterialModule } from './modules/angularmaterial/angularmaterial.module';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    TemperatureTableComponent,
    TemperatureChartComponent,
    AirHumidityTableComponent,
    AirHumidityChartComponent,
    GroundHumidityTableComponent,
    GroundHumidityChartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxEchartsModule,
    FontAwesomeModule,
    // AngularmaterialModule,
    HttpClientModule,
    DataTablesModule
  ],
  providers: [
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
